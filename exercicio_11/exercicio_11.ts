/*
Faça um programa que receba um numero positivo e
maior que zero, calcule e mostre:
a) O numero digitado ao quadrado;
b) O numero digitado ao cubo;
c) A raiz quadrada do numero digitado;
d) A raiz cubica de numero digitado.
*/

//Inicio
namespace exercicio_11
{
    //Entrada de dados 
    //let numero: number;

    const numero = 10;

    let numQ: number;

    numQ = numero * numero;
    numQ = Math.pow(numero, 2);
    numQ = numero ** 2;

    let numC: number;

    numC = numero * numero * numero;
    numC = Math.pow(numero, 3);
    numC = numero ** 3;

    let raizQ: number;

    raizQ = Math.sqrt(numero);

    let raizC: number;

    raizC = Math.cbrt(numero);

    console.log(`O numero elevado ao quadrado: ${numQ} \n O numero elevado ao cubo: ${numC} \n A raiz quadrada do numero: ${raizQ} \n A raiz cubica do numero: ${raizC}`);

    //console.log(`O numero elevado ao quadrado: ${numQ}`);
}