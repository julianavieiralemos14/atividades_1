//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o salário a receber, sabendo-se que esse funcionário tem gratificação de 5% sobre o salário base e paga imposto de 7% obre o salário-base.

namespace exercicio_6

{
    let salario_base: number;

    salario_base = 1200;

    let aumento: number;

    aumento = 5/100;

    salario_base = salario_base * (aumento/5) + salario_base;

    console.log(`Esse funcionário tem a receber: ${salario_base}`);
}


