//Faça um programa que receba o salário-base de um funcionário, calcule e mostre 
//o seu salário a receber, sabendo- se que esse funcionário tem gratificação 
//de R$50,00 e paga imposto de 10% sobre o salário-base.

namespace exercicio_7

{
    let salario_base, aumento, imposto, novo_salario: number;

    salario_base = 1500;

    imposto = salario_base * (10/100);

    aumento = 50;

    novo_salario = salario_base + aumento - imposto; 

    console.log(`O valor que ele vai receber: ${novo_salario}`)
}