//Faça um programa que receba o salário de um funcionário e o percentual de aumento, calcule e mostre o valor do aumento e o novo salário.

namespace exercicio_5

{
    let salario: number;

    salario = 1200;

    let novo_salario: number;

    let aumento: number;

    aumento = 100
    
    novo_salario = salario * (aumento/100) + salario;

    console.log(`O valor de aumento é de: ${novo_salario}`)
    console.log(`O novo salário é ${novo_salario}`);

}
